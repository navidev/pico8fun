
------------------------------------------

function _init()
    canvas = init_canvas(32,32)
    mapgen = generate_map(canvas)
end 

function _update60()
    mapgen = bsort(mapgen)   
end

function _draw()
    cls()
    draw_map(mapgen)
end
    
-------------------------------------------

function init_canvas(_width, _height)
    local canvas = {
        width = _width,
        height = _height
    }

    return canvas
end

function generate_map(_canvas)
    local pxs = {}

    for row = 0, _canvas.width do
        for col = 0, _canvas.height do
            px = {}
            px.x = row
            px.y = col
            px.color = flr(rnd(15)) - 1

            add(pxs, px)
        end    
    end

    return pxs
end

function draw_map(_mapgen)
    for i = 1, #_mapgen do 
       pset(_mapgen[i].x, _mapgen[i].y, _mapgen[i].color)
    end
end

function bsort(_mapgen)
    for i = 1, #_mapgen - 1 do
        if _mapgen[i].color > _mapgen[i + 1].color then
            _mapgen[i].color, _mapgen[i + 1].color = _mapgen[i + 1].color, _mapgen[i].color
        end
    end

    return _mapgen
end
